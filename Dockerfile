FROM gentoo/portage:latest as portage

FROM gentoo/stage3-amd64:latest

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

WORKDIR /usr/src/portage-ci
COPY . /usr/src/portage-ci

RUN /bin/sh ./provision.sh
