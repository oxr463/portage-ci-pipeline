#!/bin/sh
# Provisioning for Portage CI/CD pipeline

# Copy portage configuration
cp -R /usr/src/portage-ci/.local/etc/portage/* /etc/portage/

# Set keywords
echo 'ACCEPT_KEYWORDS="~amd64"' >> /etc/portage/make.conf

# Enable binary packages
echo 'FEATURES="buildpkg"' >> /etc/portage/make.conf

emerge -nq @portage-ci-dependencies

# Set vi as default editor
ln -s /bin/busybox /usr/local/bin/vi
eselect editor set /usr/local/bin/vi
