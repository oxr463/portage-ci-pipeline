# Portage - Continuous Integration Pipeline

## Motivation

This project began as a GitLab CI/CD pipeline implementation for the
**[rage-overlay](https://gitlab.com/oxr463/overlay)** but has been
broken out into this standalone repository for use in other overlays.

## Usage

```yml
include:
  - remote: 'https://gitlab.com/oxr463/portage-ci-pipeline/-/raw/v0.1.0/.gitlab-ci-template.yml'
```

## Acknowledgement

The [Gentoo Logo Peach Plain](https://wiki.gentoo.org/wiki/File:Gentoo-logo-peach-plain.svg)
used for the GitLab project icon is attributed to
[Matteo Pescarin](https://peach.smartart.it).

## License

SPDX-License-Identifier: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

## See Also

- [ebuildbot](https://gitlab.com/oxr463/ebuildbot)
